module A
      puts "I am a1"
      def a1
   end
end

module B
   def a2

      puts "I am a2"
   end
end


class Sample2
include A
include B

  attr_reader :name
  def initialize( name )
    @name = name
  end

   def s1
     puts "I am Sample  #@name"
     #my_lambda = -> { puts "Lambda called" }
   end


   def print_once
   yield 1
   yield 2
   yield 3
   end

 def hourlyRing
    puts "I am hourlyRing "
 end

end

ring = Proc.new do
   puts 'DING!'
end


samp = Sample2.new "cool"
samp.s1
samp.a1
samp.a2
samp.hourlyRing
samp.print_once { |number| puts number * 10 }
#my_lambda.call
