class MovieCatalog

   @@movieNames = Array.new()
   @@listOfMovieInfo = Array.new()

    def initialize(name,mvName,hash)
        @user_name=name
        @@movieNames=mvName
        @@listOfMovieInfo = hash
      end

   def displayUserName()
     puts "-----------------------------"
     puts "User name :: #@user_name"
    end

def total_no_of_movie()
     puts "-----------------------------"
     puts "Movies seen by #@user_name"
     puts "-----------------------------"

     @@movieNames.each { |x| puts x }
end

def movie_review()
   puts "-----------------------------"
   puts "Ratings and Comments given by #@user_name for each movie"
   puts "-----------------------------"
   @@listOfMovieInfo.each do |element|
      if element.class == Hash
        element.each do |key, value|
          puts "#{key} : #{value}"
        end
        puts "------"
      end
    end
end
end


class RunnerMovieCatalog


##Input command line

listOfInfo = Array.new()
puts "Enter User name :"
name = gets.chomp

puts "Enter the number of movie #{name} rated :"
n = gets.chomp.to_i

puts "Enter the list of movies #{name} seen :"
movieNames = Array.new(n)
for i in (1..n)
  movieNames[i]=gets.chomp
end

puts "Enter the list of reviews for each movie :"

for i in (1..n)
  moveName = movieNames[i]
  puts "Enter the comment for movies #{moveName} :"
  comment = gets.chomp
  puts "Enter the Raiting for movies #{moveName} :"
  raiting = gets.chomp
  review = {"movie name" => moveName, "comment" => comment,"RaitingInfo" => raiting}
  listOfInfo.push(review)
end





=begin
##Input Hard values
name = "Jonh"
movieNames = ["Hammerhead", "Great White", "Tiger"]
review1 = {"movie name" => "Hammerhead","comment" => "Good","Raiting" => "4"}
review2 = {"movie name" => "Great White","comment" => "Great","Raiting" => "2"}
review3 = {"movie name" => "Tiger","comment" => "Best","Raiting" => "3"}
listOfInfo = [review1,review2,review3]
=end

#Invoke
obj=MovieCatalog.new(name, movieNames, listOfInfo)
obj.displayUserName()
obj.total_no_of_movie()
obj.movie_review()
end
