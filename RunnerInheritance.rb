class RunnerInheritance

    # constructor of super class
    def initialize

        puts "This is Superclass"
    end

    # method of the superclass
    def super_method

        puts "Method of superclass"
    end
end

# subclass or derived class
class Sudo_Placement < RunnerInheritance

    # constructor of deriver class
    def initialize
         puts "This is Subclass"
    end
end

# creating object of superclass
RunnerInheritance.new

# creating object of subclass
sub_obj = RunnerInheritance.new

# calling the method of super
# class using sub class object
sub_obj.super_method
